<div class="center"><img src="/img/lpp.svg" alt="Le Petit Pascal - Logo complet" width="50%" title="Le Petit Pascal"/></div>

----

``` Repositery "Bescherelle" - Trophée NSI ```

*Réalisé par wilhelm43, wedego & IIllux*

Bescherelle semi-automatique générant des conjugaisons pour des verbes, même non existants.

Afin d'utiliser le Petit Pascal, il faut executer le fichier main.py à partir d'un IDLE Python.

-> infos pour lancer le projet dans le fichier  `doc`  <br>
-> code source dans le dossier `code`  <br>
-> images utilisées dans le dossier `img`  <br>
-> configuration requise dans le fichier `requirements.txt`  <br>

----
